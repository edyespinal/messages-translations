import type { ParamsContext } from '@vtex/api'
import { Service } from '@vtex/api'

import type { Clients } from './clients'
import { clientsConfig } from './clients'
import { applicationRoutes } from './middlewares/routes'
import { applicationEvents } from './middlewares/events'

export default new Service<Clients, State, ParamsContext>({
  clients: clientsConfig,
  routes: applicationRoutes,
  events: applicationEvents,
})
