import type { InstanceOptions, IOContext, IOResponse } from '@vtex/api'
import { ExternalClient } from '@vtex/api'

import type { StatusResponse } from '../typings/clients/status'

export default class Status extends ExternalClient {
  constructor(context: IOContext, options?: InstanceOptions) {
    super('http://httpstat.us', context, options)
  }

  public async getStatus(status: number): Promise<StatusResponse> {
    return this.http.get(status.toString(), {
      metric: 'status-get',
    })
  }

  public async getStatusWithHeaders(
    status: number
  ): Promise<IOResponse<StatusResponse>> {
    return this.http.getRaw(status.toString(), {
      metric: 'status-get-raw',
    })
  }

  public async getStatusAndForceMaxAge(
    status: number
  ): Promise<IOResponse<StatusResponse>> {
    return this.http.get(status.toString(), {
      forceMaxAge: 5000,
      metric: 'status-get-forceMaxAge',
    })
  }
}
