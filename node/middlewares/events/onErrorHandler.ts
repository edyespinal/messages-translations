import { IOServiceLogger } from '../../utils/errorHandling/ioServiceLogger'

export async function onErrorHandler(
  ctx: EventContext,
  next: () => Promise<void>
) {
  try {
    ctx.state.serviceLogger = new IOServiceLogger(ctx)

    await next()
  } catch (error) {
    ctx.state.serviceLogger.error(error)
  }
}
