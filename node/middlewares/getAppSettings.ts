import { APP } from '@vtex/api'

import { IOServiceError } from '../utils/errorHandling/ioServiceError'
import type { AppSettings } from '../typings/appSettings'

export async function getAppSettings(
  ctx: RouteContext | EventContext,
  next: () => Promise<void>
) {
  try {
    const {
      clients: { apps },
    } = ctx

    const settings: AppSettings = await apps.getAppSettings(APP.ID)

    if (!settings) {
      throw new Error('App settings not found')
    }

    ctx.state.appSettings = settings

    await next()
  } catch (error) {
    if (error instanceof IOServiceError) {
      throw error
    }

    throw new IOServiceError({
      message: 'Error fetching app settings',
      status: 401,
      reason: error.message,
      code: 'AUTHENTICATION_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}
