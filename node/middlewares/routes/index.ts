import { method } from '@vtex/api'

import { validate } from './validate'
import { getStatus } from './getStatus'
import { sendTestEvent } from './sendTestEvent'
import { getAppLogs } from './getAppLogs'
import { errorHandler } from './errorHandler'
import { authenticate } from './authenticate'

export const applicationRoutes = {
  status: method({
    GET: [errorHandler, validate, getStatus],
  }),
  testEvent: method({
    GET: [errorHandler, authenticate, sendTestEvent],
  }),
  appLogs: method({
    GET: [errorHandler, getAppLogs],
  }),
}
