import { IOServiceError } from '../../utils/errorHandling/ioServiceError'

export async function authenticate(
  ctx: RouteContext,
  next: () => Promise<void>
) {
  try {
    const {
      vtex: { adminUserAuthToken },
    } = ctx

    if (!adminUserAuthToken) {
      throw new Error('User not authenticated')
    }

    await next()
  } catch (error) {
    if (error instanceof IOServiceError) {
      throw error
    }

    throw new IOServiceError({
      message: 'Error authenticating user',
      status: 401,
      reason: error.message,
      code: 'AUTHENTICATION_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}
